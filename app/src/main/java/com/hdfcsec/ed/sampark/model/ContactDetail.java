package com.hdfcsec.ed.sampark.model;

public class ContactDetail {
    private Integer employeeUniqueId;
    private String employeeName;
    private String primaryNumber;
    private String alternateNumber;
    private String deskNumber;
    private String userName;
    private String gender;
    private String date_Of_Birth;
    private String branchName;
    private String department;
    private String subDepartment;
    private String designation;
    private String emailID;
    private String manager_name;
    private String manager_employee_code;
    private String imageLink;

    public ContactDetail() {
    }

    public Integer getEmployeeUniqueId() {
        return employeeUniqueId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public String getPrimaryNumber() {
        return primaryNumber;
    }

    public String getAlternateNumber() {
        return alternateNumber;
    }

    public String getDeskNumber() {
        return deskNumber;
    }

    public String getUserName() {
        return userName;
    }

    public String getGender() {
        return gender;
    }

    public String getDate_Of_Birth() {
        return date_Of_Birth;
    }

    public String getBranchName() {
        return branchName;
    }

    public String getDepartment() {
        return department;
    }

    public String getSubDepartment() {
        return subDepartment;
    }

    public String getDesignation() {
        return designation;
    }

    public String getEmailID() {
        return emailID;
    }

    public String getManager_name() {
        return manager_name;
    }

    public String getManager_employee_code() {
        return manager_employee_code;
    }

    public String getImageLink() {
        return imageLink;
    }
}
