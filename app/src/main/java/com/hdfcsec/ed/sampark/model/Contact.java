package com.hdfcsec.ed.sampark.model;

public class Contact {
    String  userName;
    String  employeeName;
    String  primaryNumber;
    String  deskNumber;
    String  imageLink;

    public Contact() {
    }

    public String getUserName() {
        return userName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public String getPrimaryNumber() {
        return primaryNumber;
    }

    public String getDeskNumber() {
        return deskNumber;
    }

    public String getImageLink() {
        return imageLink;
    }
}
