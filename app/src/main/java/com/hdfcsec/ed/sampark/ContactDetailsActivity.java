package com.hdfcsec.ed.sampark;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hdfcsec.ed.sampark.model.ContactDetail;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ContactDetailsActivity extends AppCompatActivity {
    private static final String TAG = HomeScreenActivity.class.getSimpleName();
    private String employeeCode;
    private LoadingDialog pDialog;
    private AlertDialog alertDialog;
    ContactDetail contact_details;
    private RelativeLayout rl_primary, rl_alternate, rl_desk, rl_email,rl_manager;
    private TextView tv_emp_name, tv_emp_position, tv_emp_department, tv_primary, tv_alternate, tv_desk, tv_email,tv_manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactdetails);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout ctl = findViewById(R.id.toolbar_layout);
        ctl.setTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        contact_details = new ContactDetail();
        pDialog = new LoadingDialog(ContactDetailsActivity.this);
        rl_primary = findViewById(R.id.layout_primary);
        rl_alternate= findViewById(R.id.layout_alternate);
        rl_desk =findViewById(R.id.layout_desk);
        rl_email = findViewById(R.id.layout_email);
        rl_manager = findViewById(R.id.layout_manager);

        tv_emp_name = findViewById(R.id.name);
        tv_emp_position = findViewById(R.id.designation);
        tv_emp_department = findViewById(R.id.department);
        tv_primary = findViewById(R.id.tvNumber1);
        tv_alternate = findViewById(R.id.tvNumber2);
        tv_desk = findViewById(R.id.tvNumber3);
        tv_email = findViewById(R.id.tvNumber4);
        tv_manager = findViewById(R.id.tvNumber5);


        Intent intent=getIntent();
        employeeCode= intent.getStringExtra("employee_code");
        try {
            fetchContacts(employeeCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    // url to fetch contacts json
    private void fetchContacts(final String employeeCode) throws JSONException {
        pDialog.showLoadingDialog();
        JSONObject postData = new JSONObject();
        postData.put("employeeCode", employeeCode);
        Log.i(TAG, postData.toString());

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST,
                getResources().getString(R.string.BASE_URL)+getResources().getString(R.string.Employee_Details)
                , postData, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, response.toString());
                pDialog.hideLoadingDialog();
                if (response == null) {
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the contacts! Pleas try again.", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    JSONObject data = response.getJSONObject("data");
                    contact_details = new Gson().fromJson(data.toString(), new TypeToken<ContactDetail>() {
                    }.getType());
                    tv_emp_name.setText(contact_details.getEmployeeName());
                    tv_emp_position.setText(contact_details.getDesignation());
                            tv_emp_department.setText(contact_details.getDepartment());
                    tv_primary.setText(contact_details.getPrimaryNumber());
                    tv_alternate.setText(contact_details.getAlternateNumber());
                    tv_desk.setText(contact_details.getDeskNumber());
                    tv_email.setText(contact_details.getEmailID());
                    tv_manager.setText(contact_details.getManager_name());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.i(TAG,error.toString());
                pDialog.hideLoadingDialog();
                Toast.makeText(ContactDetailsActivity.this, "Fail to Update", Toast.LENGTH_SHORT).show();
            }
        });
        MyApplication.getInstance().addToRequestQueue(objectRequest);
    }
}
